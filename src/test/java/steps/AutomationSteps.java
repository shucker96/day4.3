package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class AutomationSteps extends TestRunner {
    public ChromeDriver driver;
//    public url = "http://www.automationpractice.com";

    @Given("I setup driver")
    public void iSetupDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
        ChromeOptions chromeoptions = new ChromeOptions();
        driver = new ChromeDriver(chromeoptions);
        driver.manage().window().maximize();
    }

    @And("I go to {string}")
    public void iGoTo(String url) {
        driver.get(url);
    }
}
